import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AliciaComponent } from './alicia.component';

describe('AliciaComponent', () => {
  let component: AliciaComponent;
  let fixture: ComponentFixture<AliciaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AliciaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AliciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
