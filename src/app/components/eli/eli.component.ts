import { Component } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-eli',
  templateUrl: './eli.component.html',
  styleUrls: ['./eli.component.css']
})
export class EliComponent {
  constructor(public modal:NgbModal) { }

  ngOnInit(): void {
  }
}
