import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaniaComponent } from './tania.component';

describe('TaniaComponent', () => {
  let component: TaniaComponent;
  let fixture: ComponentFixture<TaniaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaniaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TaniaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
