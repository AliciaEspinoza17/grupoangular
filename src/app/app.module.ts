import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AliciaComponent } from './components/alicia/alicia.component';
import { TaniaComponent } from './components/tania/tania.component';
import { DenyComponent } from './components/deny/deny.component';
import { EliComponent } from './components/eli/eli.component';
import { AntonioComponent } from './components/antonio/antonio.component';
import { GustavoComponent } from './components/gustavo/gustavo.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [
    AppComponent,
    AliciaComponent,
    TaniaComponent,
    DenyComponent,
    EliComponent,
    AntonioComponent,
    GustavoComponent
  ],
  imports: [
    BrowserModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
